<?php if ($formModel->isLocked()): ?>
<p>
    <strong>
        🔒 <?= e(trans('studiobosco.lockrecords::lang.messages.locked_by', ['editor_name' => $formModel->getLock()->editor->full_name ?: $formModel->getLock()->editor->login])); ?> -
        <?= Backend::dateTime($formModel->getLock()->created_at); ?>
    </strong>
</p>
<?php endif; ?>
