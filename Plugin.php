<?php namespace StudioBosco\LockRecords;

use Event;
use Backend;
use Backend\Models\UserRole;
use System\Classes\PluginBase;

/**
 * LockRecords Plugin Information File
 */
class Plugin extends PluginBase
{
    /**
     * Returns information about this plugin.
     */
    public function pluginDetails(): array
    {
        return [
            'name'        => 'studiobosco.lockrecords::lang.plugin.name',
            'description' => 'studiobosco.lockrecords::lang.plugin.description',
            'author'      => 'StudioBosco',
            'icon'        => 'icon-leaf'
        ];
    }

    /**
     * Register method, called when the plugin is first registered.
     */
    public function register(): void
    {

    }

    /**
     * Boot method, called right before the request route.
     */
    public function boot(): void
    {

    }

    /**
     * Registers any frontend components implemented in this plugin.
     */
    public function registerComponents(): array
    {
        return []; // Remove this line to activate

        return [
            'StudioBosco\LockRecords\Components\MyComponent' => 'myComponent',
        ];
    }

    /**
     * Registers any backend permissions used by this plugin.
     */
    public function registerPermissions(): array
    {
        return [
            'studiobosco.lockrecords::manually_unlock_records' => [
                'tab' => 'studiobosco.lockrecords::lang.plugin.name',
                'label' => 'studiobosco.lockrecords::lang.permissions.manually_unlock_records',
            ],
            'studiobosco.lockrecords::edit_settings' => [
                'tab' => 'studiobosco.lockrecords::lang.plugin.name',
                'label' => 'studiobosco.lockrecords::lang.permissions.edit_settings',
            ],
        ];
    }

    /**
     * Registers backend navigation items for this plugin.
     */
    public function registerNavigation(): array
    {
        return []; // Remove this line to activate

        return [
            'lockrecords' => [
                'label'       => 'studiobosco.lockrecords::lang.plugin.name',
                'url'         => Backend::url('studiobosco/lockrecords/mycontroller'),
                'icon'        => 'icon-lock',
                'permissions' => ['studiobosco.lockrecords.*'],
                'order'       => 500,
            ],
        ];
    }

    public function registerSettings()
    {
        return [
            'settings' => [
                'label' => 'studiobosco.lockrecords::lang.plugin.name',
                'description' => 'studiobosco.lockrecords::lang.settings.description',
                'icon' => 'icon-lock',
                'class' => 'StudioBosco\LockRecords\Models\Settings',
                'order' => 100,
                'permissions' => [
                    'studiobosco.lockrecords::edit_settings',
                ],
            ],
        ];
    }
}
