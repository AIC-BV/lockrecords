<?php namespace StudioBosco\LockRecords\Models;

use Model;

/**
 * LockedRecord Model
 */
class LockedRecord extends Model
{
    use \Winter\Storm\Database\Traits\Validation;

    /**
     * @var string The database table used by the model.
     */
    public $table = 'studiobosco_locked_records';

    /**
     * @var array Guarded fields
     */
    protected $guarded = ['*'];

    /**
     * @var array Fillable fields
     */
    protected $fillable = [
        'record_id',
        'record_type',
        'editor_id',
    ];

    /**
     * @var array Validation rules for attributes
     */
    public $rules = [
        'record_id' => 'required',
        'record_type' => 'required',
        'editor_id' => 'required',
    ];

    /**
     * @var array Attributes to be cast to native types
     */
    protected $casts = [];

    /**
     * @var array Attributes to be cast to JSON
     */
    protected $jsonable = [];

    /**
     * @var array Attributes to be appended to the API representation of the model (ex. toArray())
     */
    protected $appends = [];

    /**
     * @var array Attributes to be removed from the API representation of the model (ex. toArray())
     */
    protected $hidden = [];

    /**
     * @var array Attributes to be cast to Argon (Carbon) instances
     */
    protected $dates = [
        'created_at',
        'updated_at',
    ];

    /**
     * @var array Relations
     */
    public $hasOne = [];
    public $hasMany = [];
    public $hasOneThrough = [];
    public $hasManyThrough = [];
    public $belongsTo = [
        'editor' => \Backend\Models\User::class,
    ];
    public $belongsToMany = [];
    public $morphTo = [
        'record' => [],
    ];
    public $morphOne = [];
    public $morphMany = [];
    public $attachOne = [];
    public $attachMany = [];

    public function beforeSave()
    {
        // prevent having multiple locks for the same record
        if ($this->record) {
            $existing = self::forRecord($this->record);

            // ignore this lock in query (if it saved yet)
            if ($this->id) {
                $existing->whereNot('id', $this->id);
            }

            $existing = $existing->get();

            // delete all existing locks for this record
            foreach($existing as $lock) {
                $lock->delete();
            }
        }
    }

    public function scopeForRecord($query, $record)
    {
        if (!$record) {
            return $query->limit(0);
        }

        return $query->where('record_id', $record->id)->where('record_type', $record::class);
    }
}
