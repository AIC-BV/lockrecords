<?php namespace StudioBosco\LockRecords\Behaviors;

use BackendAuth;
use Winter\Storm\Extension\ExtensionBase;
use StudioBosco\LockRecords\Models\LockedRecord;

class LockableModel extends ExtensionBase
{
    use \StudioBosco\LockRecords\Traits\Lockable;

    protected $model;

    public function __construct($parent)
    {
        $this->model = $parent;
        $this->bootLockable();
    }

    public function lock($user = null)
    {
        $user = $user ?: BackendAuth::getUser();

        if (!$user) {
            return;
        }

        if ($this->isLocked($user)) {
            return;
        }

        $lock = $this->model->recordLock ?: new LockedRecord();
        $lock->record = $this->model;
        $lock->editor = $user;
        $lock->touch();
        $lock->save();
    }

    public function getLock()
    {
        return $this->model->recordLock;
    }

    public function bootLockable()
    {
        // attach relationship
        $this->model::extend(function ($model) {
            $model->morphOne['recordLock'] = [
                LockedRecord::class,
                'name' => 'record',
            ];
        });

        // prevent model from updating if it is locked
        $this->model::updating(function ($model) {
            if ($model->isLocked()) {

                Log::warning('Record of type ' . $model::class . ' with ID ' . $model->id . ' is locked by user ' . $model->getLock()->editor->login . ' and cannot be updated.');

                $user = BackendAuth::getUser();

                if ($user) {
                    Flash::warning(trans('studiobosco.lockrecords::plugin.messages.record_locked_when_updating', ['record_name' => $model::class, 'record_id' => $model->id, 'editor_name' => $model->getLock()->editor->login]));
                }
                return false;
            }
        });
    }
}
