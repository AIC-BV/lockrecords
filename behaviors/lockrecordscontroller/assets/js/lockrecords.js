window.addEventListener('beforeunload', function (event) {
    delete event['returnValue'];
    const form = document.getElementById('Form');
    $(form).request('onUnlockRecord');
}, false);

const form = document.getElementById('Form');
$(form).request('onLockRecord');

window.setInterval(function () {
    $(form).request('onLockRecord');
}, 5000);
