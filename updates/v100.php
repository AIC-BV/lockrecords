<?php namespace StudioBosco\LockRecords\Updates;

use Schema;
use Winter\Storm\Database\Schema\Blueprint;
use Winter\Storm\Database\Updates\Migration;

class V100 extends Migration
{
    public function up()
    {
        Schema::create('studiobosco_locked_records', function (Blueprint $table) {
            $table->engine = 'InnoDB';
            $table->increments('id');
            $table->bigInteger('editor_id')->unsigned();
            $table->bigInteger('record_id')->unsigned();
            $table->string('record_type', 1024);
            $table->timestamps();
        });
    }

    public function down()
    {
        Schema::dropIfExists('studiobosco_locked_records');
    }
}
