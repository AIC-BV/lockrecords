# Lock Records plugin for WinterCMS

Provides lockable records in the backend. Lockabe records can only be edited by a single backend user at the same time.
Locked records can be unlocked manually or will be unlocked automatically after a certain amount of time if no editing form is opened.

## Usage

This plugin provides the `StudioBosco\LockRecords\Traits\Lockable` Model Trait and the `StudioBosco\LockRecords\Behaviors\LockRecordsController` controller behavior.

To make a model lockable

1. Just add the trait or the behavior:

```php
class MyModel {
    use \StudioBosco\LockRecords\Traits\Lockable;

    // or

    public $implement = [
        \StudioBosco\LockRecords\Behaviors\LockableModel::class,
    ];

    ...
}
```

2. Add the behavior to the backend controller. It should contain the form and list behaviors.

```php
class MyController extends Backend\Classes\Controller {

    /**
     * @var array Behaviors that are implemented by this controller.
     */
    public $implement = [
        \Backend.Behaviors.FormController::class,
        \Backend.Behaviors.ListController::class,
        \StudioBosco\LockRecords\Behaviors\LockRecordsController::class,
    ];

    ...
}
```

3. Customize the duration after which records get unlocked automatically by visiting `backend/system/settings/update/studiobosco/lockrecords/settings`.
   It is 10 minutes by default.

4. Set permissions to manually unlock records if desired.
