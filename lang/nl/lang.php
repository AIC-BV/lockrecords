<?php

return [
    'plugin' => [
        'name' => 'Records vergrendelen',
        'description' => 'Staat toe om automatisch records te vergrendelen voor andere backend gebruikers.',
        'namespace' => '',
    ],
    'permissions' => [
        'auto_lock_records' => 'Vergrendel records wanneer iemand dit aan het wijzigen is.',
        'manually_unlock_records' => 'Ontgrendel records handmatig',
        'edit_settings' => 'Beheer instellingen',
    ],
    'messages' => [
        'unlock_record' => 'Vergrendeling opheffen',
        'unlock_record_confirm' => 'Ben je zeker dat je de vergrendeling wil opheffen?',
        'unlock_record_success' => 'Record ontgrendeld.',
        'locked_at' => 'Vergrendeld op',
        'locked_by' => 'Vergrendeld door :editor_name',
        'record_locked_when_updating' => 'Record :record_name met ID :record_id is vergrendeld door :editor_name en kan niet bewaard worden.'
    ],
    'settings' => [
        'description' => 'Instellingen om records te vergrendelen.',
        'max_age' => 'Aantal minuten tot automatische ontgrendeling.',
    ],
];
